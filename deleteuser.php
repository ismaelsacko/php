
<?php
if (empty($_GET['id']) || !ctype_digit($_GET['id'])) {
    die("Oh oh ! Tu n'as pas précisé l'ID de l'utilisateur !");
}

$id = $_GET['id'];

require_once 'lib/database.php';
$users = $pdo->prepare('SELECT * FROM user WHERE id = :id');
$users->execute(compact('id'));

if ($users->rowCount() === 0) {
    die("Cet utilisateur n'exite pas. Vous ne pouvez donc pas le supprimer");
}
$users = $pdo->prepare('DELETE FROM user WHERE id = :id');
$users->execute(compact('id'));
header('Location: index.php');
exit();